package mssqlserver

import(
    "fmt"
    "database/sql"
    "net/url"
    _ "github.com/denisenkom/go-mssqldb"
    "github.com/elastic/beats/metricbeat/mb"
    "github.com/elastic/beats/libbeat/logp"
    "github.com/pkg/errors"
)

func init(){
    if err := mb.Registry.AddModule("mssqlserver", NewModule); err != nil{
        panic(err)
    }
}

func NewModule(base mb.BaseModule) (mb.Module, error){
    
    config := struct{
        Hosts []string `config:"hosts"
            validate:"nonzero,required"`
        Username string `config:"username"
            validate:"nonzero,required"`
        Password string `config:"password"
            validate:"nonzero,required"`
        Port string `config:"port"
            validate:"nonzero,required"`
        Database string `config:"database"
            validate:"nonzero,required"`
    }{}
    if err := base.UnpackConfig(&config); err != nil {
        return nil, err
    }

    return &base, nil
}

// NewDB returns a new oracle database handle. The dsn value (data source name)
// must be valid, otherwise an error will be returned.
//
//   DSN Format: username/password@host:port/service_name
func NewDB(dsnString string, sanitizedUri string) (*sql.DB, error){
    logp.Info("Opening connection to " + sanitizedUri)

    //Open DB connection
    oConn, err := sql.Open("sqlserver",dsnString)
    if err != nil{
        logp.Err("Couldn't open connection to " + sanitizedUri)
        return oConn, errors.Wrap(err, "sql open failed")
    }
    logp.Info("Connection opened to " + sanitizedUri)
    return oConn, nil
}

func FormatDSN(mod mb.Module, host string) (mb.HostData, error){
    config := struct{
        Hosts []string `config:"hosts"
            validate:"nonzero,required"`
        Username string `config:"username"
            validate:"nonzero,required"`
        Password string `config:"password"
            validate:"nonzero,required"`
        Port string `config:"port"
            validate:"nonzero,required"`
        Database string `config:"database"
            validate:"nonzero,required"`
        ConnectionTimeout string `config:"connectionTimeout"
            validate:"nonzero,required"`
    }{}
    if err := mod.UnpackConfig(&config); err != nil {
        return mb.HostData{}, err
    }

    query := url.Values{}
    query.Add("database", config.Database)
    query.Add("connection timeout", fmt.Sprintf("%s", config.ConnectionTimeout))

    u := &url.URL{
        Scheme:   "sqlserver",
        User:     url.UserPassword(config.Username, config.Password),
        Host:     fmt.Sprintf("%s:%s", config.Hosts[0], config.Port),
//        Path:  "database="config.Database, // if connecting to an instance instead of a port
        RawQuery: query.Encode(),
    }

    connectionString := u.String()

    sanitizedUri := "sqlserver://"+config.Username+"@"+config.Hosts[0]+":"+config.Port+"/"+config.Database

    return mb.HostData{
        URI:          connectionString,
        SanitizedURI: sanitizedUri,
        Host:         config.Hosts[0],
        User:         config.Username,
        Password:     config.Password,
    }, nil
}