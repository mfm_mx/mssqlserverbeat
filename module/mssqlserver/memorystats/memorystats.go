package memorystats

import (
    "strings"
    "database/sql"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/metricbeat/mb"

    sqlserverbeat "bitbucket.org/mfm_mx/mssqlserverbeat/module/mssqlserver"
    "github.com/pkg/errors"
    "github.com/elastic/beats/libbeat/logp"	
)

// init registers the MetricSet with the central registry.
// The New method will be called after the setup of the module and before starting to fetch data
func init() {
	if err := mb.Registry.AddMetricSet("mssqlserver", "memorystats", New, sqlserverbeat.FormatDSN); err != nil {
		panic(err)
	}
}

// MetricSet type defines all fields of the MetricSet
// As a minimum it must inherit the mb.BaseMetricSet fields, but can be extended with
// additional entries. These variables can be used to persist data or configuration between
// multiple fetch calls.
type MetricSet struct {
	mb.BaseMetricSet
	counter int
}

// New create a new instance of the MetricSet
// Part of new is also setting up the configuration by processing additional
// configuration entries if needed.
func New(base mb.BaseMetricSet) (mb.MetricSet, error) {

	config := struct{}{}

	if err := base.Module().UnpackConfig(&config); err != nil {
		return nil, err
	}

	return &MetricSet{
		BaseMetricSet: base,
		counter:       1,
	}, nil
}

// Fetch methods implements the data gathering and data conversion to the right format
// It returns the event which is then forward to the output. In case of an error, a
// descriptive error must be returned.
func (m *MetricSet) Fetch() (common.MapStr, error) {

	sqlServerDB, err := sqlserverbeat.NewDB(m.HostData().URI, m.HostData().SanitizedURI)

    defer sqlServerDB.Close()

    if err != nil{
//    	panic(err)
        failEvent:= common.MapStr{}
        failEvent.Put("status","OFFLINE")
        return failEvent, errors.Wrap(err, "memorystats open db connection failed")
    }

    memory, err := m.loadMemoryStatsFromOsProcessMemory(sqlServerDB)

    if err != nil{
    	return nil, err
    }

    event := eventMapping(memory)

	return event, nil
}

/*
*  Returns information about SQL Server memory performance invoking dm_os_performance_counters
*  User requires VIEW SERVER STATE permission.
* 
*  https://www.sqlshack.com/sql-server-memory-performance-metrics-part-2-available-bytes-total-server-target-server-memory/
*
*  https://docs.microsoft.com/en-us/sql/relational-databases/system-dynamic-management-views/sys-dm-os-performance-counters-transact-sql
*/
func (m *MetricSet) loadMemoryStats(db *sql.DB) (map[string]string, error){
    rows, err := db.Query("SELECT counter_name, cntr_value FROM sys.dm_os_performance_counters where (counter_name like '%Total Server Memory%' OR counter_name like '%Target Server Memory%');;")
    if err != nil{
    	logp.Err("Query execution error", err)
        return nil, err
    }

    defer rows.Close()

    memoryStatus := map[string]string{}

    for rows.Next(){
        var counter_name string
        var cntr_value string
        err = rows.Scan(&counter_name, &cntr_value)
        if err != nil{
            return nil, err
        }

        if strings.Contains(counter_name, "Target"){
            memoryStatus["target_server_memory_kb"] = cntr_value
        }else if strings.Contains(counter_name, "Total"){
            memoryStatus["total_server_memory_kb"] = cntr_value	
        }
    }
    return memoryStatus, nil
}

/*
*  Returns information about SQL Server memory performance invoking dm_os_process_memory
*  User requires VIEW SERVER STATE permission.
* 
*  https://docs.microsoft.com/en-us/sql/relational-databases/performance-monitor/monitor-memory-usage
*
*/
func (m *MetricSet) loadMemoryStatsFromOsProcessMemory(db *sql.DB) (map[string]string, error){
    rows, err := db.Query("SELECT " +
    " (physical_memory_in_use_kb/1024) AS memory_used_mb, "+
    "(locked_page_allocations_kb/1024) AS locked_pages_mb, "+
    "(total_virtual_address_space_kb/1024) AS total_vas_mb, "+
    "process_physical_memory_low, "+
    "process_virtual_memory_low "+
    "FROM sys.dm_os_process_memory;");

    if err != nil{
        logp.Err("Query execution error", err)
        return nil, err
    }

    defer rows.Close()

    memoryStatus := map[string]string{}

    for rows.Next(){
        var memory_used_mb string
        var locked_pages_mb string
        var total_vas_mb string
        var process_physical_memory_low string
        var process_virtual_memory_low string

        err = rows.Scan(&memory_used_mb, &locked_pages_mb, &total_vas_mb, &process_physical_memory_low, &process_virtual_memory_low)
        if err != nil{
            return nil, err
        }
        memoryStatus["memory_used_mb"] = memory_used_mb
        memoryStatus["locked_pages_mb"] = locked_pages_mb
        memoryStatus["total_vas_mb"] = total_vas_mb
        memoryStatus["process_physical_memory_low"] = process_physical_memory_low
        memoryStatus["process_virtual_memory_low"] = process_virtual_memory_low
    }
    return memoryStatus, nil
}