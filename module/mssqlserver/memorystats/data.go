package memorystats

import(
	"github.com/elastic/beats/libbeat/common"
	s "github.com/elastic/beats/metricbeat/schema"
	c "github.com/elastic/beats/metricbeat/schema/mapstrstr"
)

var (
    schema = s.Schema{
        "memory_used_mb": c.Int("memory_used_mb"),
        "locked_pages_mb": c.Int("locked_pages_mb"),
        "total_vas_mb": c.Int("total_vas_mb"),
        "process_physical_memory_low": c.Bool("process_physical_memory_low"),
        "process_virtual_memory_low": c.Bool("process_virtual_memory_low"),
    }
)

func eventMapping(status map[string]string) common.MapStr{
	source := map[string]interface{}{}
//    event := common.MapStr{}
	for key, value := range status{
		source[key] = value
//		event.Put(key,c.Int(value))
	}
    return schema.Apply(source)
}