package volumestats

import (
    "database/sql"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/metricbeat/mb"

    sqlserverbeat "bitbucket.org/mfm_mx/mssqlserverbeat/module/mssqlserver"
    "github.com/pkg/errors"
    "github.com/elastic/beats/libbeat/logp"
)

// init registers the MetricSet with the central registry.
// The New method will be called after the setup of the module and before starting to fetch data
func init() {
	if err := mb.Registry.AddMetricSet("mssqlserver", "volumestats", New, sqlserverbeat.FormatDSN); err != nil {
		panic(err)
	}
}

// MetricSet type defines all fields of the MetricSet
// As a minimum it must inherit the mb.BaseMetricSet fields, but can be extended with
// additional entries. These variables can be used to persist data or configuration between
// multiple fetch calls.
type MetricSet struct {
	mb.BaseMetricSet
	counter int
}

// New create a new instance of the MetricSet
// Part of new is also setting up the configuration by processing additional
// configuration entries if needed.
func New(base mb.BaseMetricSet) (mb.MetricSet, error) {

	config := struct{}{}

	if err := base.Module().UnpackConfig(&config); err != nil {
		return nil, err
	}

	return &MetricSet{
		BaseMetricSet: base,
		counter:       1,
	}, nil
}

// Fetch methods implements the data gathering and data conversion to the right format
// It returns the event which is then forward to the output. In case of an error, a
// descriptive error must be returned.
func (m *MetricSet) Fetch() ([]common.MapStr, error) {

	sqlServerDB, err := sqlserverbeat.NewDB(m.HostData().URI, m.HostData().SanitizedURI)

    defer sqlServerDB.Close()

    if err != nil{
//    	panic(err)
        failEvent:= []common.MapStr{}
        failEvent[0].Put("status","OFFLINE")
        return failEvent, errors.Wrap(err, "volumestats open db connection failed")
    }

    volumes, err := m.loadVolumeStats(sqlServerDB)

    if err != nil{
    	return nil, err
    }

	events := eventMapping(volumes)

	return events, nil
}

/*
*  Returns information about the operating system volume (directory) on which the specified 
*  databases and files are stored in SQL Server
*  User requires VIEW SERVER STATE permission.
*  https://docs.microsoft.com/en-us/sql/relational-databases/system-dynamic-management-views/sys-dm-os-volume-stats-transact-sql
*/
func (m *MetricSet) loadVolumeStats(db *sql.DB) ([]map[string]string, error){
    rows, err := db.Query("SELECT f.name, volume_mount_point, total_bytes, available_bytes FROM sys.database_files AS f CROSS APPLY sys.dm_os_volume_stats(DB_ID(f.name), f.file_id);")
    if err != nil{
    	logp.Err("Query execution error", err)
        return nil, err
    }

    defer rows.Close()

    var volumes [] map[string]string

    for rows.Next(){
        volumeStatus := map[string]string{}
        var name string
        var volume_mount_point string
        var total_bytes string
        var available_bytes string
        err = rows.Scan(&name, &volume_mount_point, &total_bytes, &available_bytes)
        if err != nil{
            return nil, err
        }

        volumeStatus["name"] = name
        volumeStatus["volume_mount_point"] = volume_mount_point
        volumeStatus["total_bytes"] = total_bytes
        volumeStatus["available_bytes"] = available_bytes
        volumes = append(volumes,volumeStatus)
    }

    return volumes, nil
}