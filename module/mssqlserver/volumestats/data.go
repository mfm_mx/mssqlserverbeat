package volumestats

import(
    "github.com/elastic/beats/libbeat/common"
    s "github.com/elastic/beats/metricbeat/schema"
    c "github.com/elastic/beats/metricbeat/schema/mapstrstr"
)

var (
    schema = s.Schema{
        "name": c.Str("name"),
        "total_bytes": c.Int("total_bytes"),
        "available_bytes": c.Int("available_bytes"),
        "volume_mount_point": c.Str("volume_mount_point"),
    }
)

func eventMapping(status []map[string]string) []common.MapStr{
    var events []common.MapStr

    for _, item := range status{
    	source := map[string]interface{}{}
    	for key, value := range item{
    		source[key] = value
    	}
        event := schema.Apply(source)
		events = append(events, event)
    }
    return events
}
